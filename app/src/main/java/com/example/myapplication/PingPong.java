package com.example.myapplication;

import android.app.Application;
import android.content.Context;

import com.example.myapplication.util.LocaleHelper;
import com.onesignal.OneSignal;

public class PingPong extends Application {
    private static final String ONESIGNAL_APP_ID = "0f7a1f96-50aa-4b15-83a9-5baa1014d5d4";

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base, "ru"));
    }

    @Override
    public void onCreate() {
        super.onCreate();
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE);
        OneSignal.initWithContext(this);
        OneSignal.setAppId(ONESIGNAL_APP_ID);
    }

}
