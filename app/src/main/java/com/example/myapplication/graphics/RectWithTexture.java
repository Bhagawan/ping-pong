package com.example.myapplication.graphics;

import android.opengl.GLES20;
import android.opengl.Matrix;

import com.example.myapplication.util.Shaders;
import com.example.myapplication.util.Textures;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

public class RectWithTexture {
    private static FloatBuffer vertexBuffer, texBuffer;
    private static ShortBuffer drawListBuffer;
    private static int mProgram;

    private int positionHandle;
    private int colorHandle;

    private int vPMatrixHandle, texPositionHandle, texHandle;
    private static final int COORDS_PER_VERTEX = 3;

    private static float[] racketCoords = {
            0.5f, -0.5f, 0.0f,
            0.5f, 0.5f, 0.0f,
            -0.5f, 0.5f, 0.0f,
            -0.5f, -0.5f, 0.0f};

    private static float[] texCoords = {
            1.0f, 1.0f, 0.0f,
            1.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 0.0f,
            0.0f, 1.0f, 0.0f
    };


    private final float[] transformMatrix = new float[16];
    private final float[] rotateMatrix = new float[16];
    private final float[] translateMatrix = new float[16];
    private final float[] scaleMatrix = new float[16];
    private static final int vertexStride = COORDS_PER_VERTEX * 4;
    private static final short[] drawOrder = { 0, 1, 2, 3 };
    private final float[] color = { 1.0f, 1.0f, 1.0f, 1.0f };
    private float mWidth, mHeight;
    private int mTexture;

    static {
        ByteBuffer bb = ByteBuffer.allocateDirect(racketCoords.length * 4);
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put(racketCoords);
        vertexBuffer.position(0);

        ByteBuffer dlb = ByteBuffer.allocateDirect(drawOrder.length * 2);
        dlb.order(ByteOrder.nativeOrder());
        drawListBuffer = dlb.asShortBuffer();
        drawListBuffer.put(drawOrder);
        drawListBuffer.position(0);

        ByteBuffer tbb = ByteBuffer.allocateDirect(texCoords.length * 4);
        tbb.order(ByteOrder.nativeOrder());
        texBuffer = tbb.asFloatBuffer();
        texBuffer.put(texCoords);
        texBuffer.position(0);

        mProgram = Shaders.textureProgram;
    }

    public RectWithTexture(float width, float height, int texture) {
        setSize(width * 2, height);
        setAngle(0);
        setPosition(0,0);
        mTexture = texture;
    }

    public void draw(float[] mvpMatrix , float x, float y) {
        setPosition(x, y);
        draw(mvpMatrix);
    }

    public void draw(float[] mvpMatrix) {
        GLES20.glUseProgram(mProgram);

        Matrix.setIdentityM(transformMatrix, 0);
        Matrix.multiplyMM(transformMatrix, 0, mvpMatrix, 0, rotateMatrix, 0);
        Matrix.multiplyMM(transformMatrix, 0, transformMatrix, 0, translateMatrix, 0);
        Matrix.multiplyMM(transformMatrix, 0, transformMatrix, 0, scaleMatrix, 0);

        positionHandle = GLES20.glGetAttribLocation(mProgram, "aPosition");
        GLES20.glEnableVertexAttribArray(positionHandle);
        GLES20.glVertexAttribPointer(positionHandle, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                vertexStride, vertexBuffer);

        colorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");
        GLES20.glUniform4fv(colorHandle, 1, color, 0);

        texPositionHandle = GLES20.glGetAttribLocation(mProgram, "aTexCoordinate");
        GLES20.glEnableVertexAttribArray(texPositionHandle);
        GLES20.glVertexAttribPointer(texPositionHandle, COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, vertexStride , texBuffer);

        texHandle = GLES20.glGetUniformLocation(mProgram, "uTexture");
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTexture);
        GLES20.glUniform1i(texHandle, 0);


        vPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");
        GLES20.glUniformMatrix4fv(vPMatrixHandle, 1, false, transformMatrix, 0);

        GLES20.glDrawElements(GLES20.GL_TRIANGLE_FAN, drawOrder.length, GLES20.GL_UNSIGNED_SHORT, drawListBuffer);

        GLES20.glDisableVertexAttribArray(positionHandle);
        GLES20.glDisableVertexAttribArray(texPositionHandle);
    }

    public void setColor(float red, float green, float blue, float alpha) {
        color[0] = red;
        color[1] = green;
        color[2] = blue;
        color[3] = alpha;
    }

    public void setTexture(int texture) {
        mTexture = texture;
    }

    public void setSize(float width, float height) {
        this.mWidth = width;
        this.mHeight = height;
        Matrix.setIdentityM(scaleMatrix, 0);
        Matrix.scaleM(scaleMatrix, 0, width, height, 1);
    }

    public void setPosition(float x, float y) {
        Matrix.setIdentityM(translateMatrix, 0);
        Matrix.translateM(translateMatrix, 0, x, y, 0);
    }

    public void setAngle(float angle) {
        Matrix.setRotateM(rotateMatrix, 0, angle,0.0f, 0.0f, 1.0f);
    }

    public float getWidth() {
        return mWidth;
    }

    public float getHeight() {
        return mHeight;
    }
}
