package com.example.myapplication.graphics;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.example.myapplication.game.Game;

public class MyGLSurfaceView extends GLSurfaceView {

    private MyGLRenderer renderer;
    private Game game;
    private GameInterface callback;

    public MyGLSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setEGLContextClientVersion(2);
        game = new Game();
        renderer = new MyGLRenderer(getContext(), game);
        setPreserveEGLContextOnPause(true);
        renderer.setCallback(() -> {
            if(callback != null) callback.onTexturesLoaded();
        });

        setRenderer(renderer);
        game.setCallback(redPlayerWins -> {
            if(callback != null) callback.onWin(redPlayerWins);
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getActionMasked();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN:
                game.moveRacket(event.getX(), event.getY());
                break;
            case MotionEvent.ACTION_MOVE:
                for(int i = 0; i < event.getPointerCount(); i++) {
                    game.moveRacket(event.getX(i), event.getY(i));
                }
                break;
            case MotionEvent.ACTION_UP:
        }
        return true;
    }

    public void setCallback(GameInterface callback) {
        this.callback = callback;
    }

    public void setGameMode(boolean onePlayer) { game.setMode(onePlayer);}

    public void newGame() {game.newGame();}

    public interface GameInterface {
        void onWin(boolean redPlayerWins);

        void onTexturesLoaded();
    }

}
