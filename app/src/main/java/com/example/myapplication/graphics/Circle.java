package com.example.myapplication.graphics;

import android.opengl.GLES20;
import android.opengl.Matrix;

import com.example.myapplication.util.Shaders;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class Circle {
    private FloatBuffer vertexBuffer;

    private int vertexCount;
    private final int COORDS_PER_VERTEX = 3;
    private int vertexStride = COORDS_PER_VERTEX * 4;

    private float radius;
    private int count;

    private float x;
    private float y;
    private float z;

    private int positionHandle;
    private int colorHandle;
    float[] color = {1.0f, 1.0f, 1.0f, 1.0f};
    private int mProgram;
    private int mVPMatrixHandle;

    public Circle(float radius, int count, float x, float y, float z) {
        this.radius = radius;
        this.count = count;
        this.x = x;
        this.y = y;
        this.z = z;

        ByteBuffer bb = ByteBuffer.allocateDirect((count + 2) * COORDS_PER_VERTEX * 4);
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();
        float[] coords = circleCoords();

        vertexBuffer.put(coords);
        vertexBuffer.position(0);

        mProgram = Shaders.simpleProgram;
    }

    private float[] circleCoords() {
        vertexCount = count + 2;
        float[] coords = new float[vertexCount * COORDS_PER_VERTEX];
        int offset = 0;
        coords[offset++] = x;
        coords[offset++] = y;
        coords[offset++] = z;
        for (int i = 0; i < count + 1; i++) {
            float angleInRadians = ((float) i / (float) count) * ((float) Math.PI * 2f);
            coords[offset++] = x + radius * (float) Math.sin(angleInRadians);
            coords[offset++] = y + radius * (float) Math.cos(angleInRadians);
            coords[offset++] = z;
        }
        return coords;
    }

    public void draw(float[] mVPMatrix, float x, float y) {
        GLES20.glUseProgram(mProgram);
        float[] mMMatrix = mVPMatrix.clone();
        Matrix.translateM(mMMatrix, 0, x, y, 0);

        positionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition");
        GLES20.glEnableVertexAttribArray(positionHandle);
        GLES20.glVertexAttribPointer(positionHandle, COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, vertexStride, vertexBuffer);
        colorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");
        GLES20.glUniform4fv(colorHandle, 1, color, 0);
        mVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");
        GLES20.glUniformMatrix4fv(mVPMatrixHandle, 1, false, mMMatrix, 0);

        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_FAN, 0, vertexCount);

        GLES20.glDisableVertexAttribArray(positionHandle);
    }

    public void SetColor(float red, float green, float blue, float alpha) {
        color[0] = red;
        color[1] = green;
        color[2] = blue;
        color[3] = alpha;
    }

    public float getRadius() {
        return radius;
    }
}
