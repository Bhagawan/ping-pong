package com.example.myapplication.graphics;

import android.opengl.GLES20;
import android.opengl.Matrix;

import com.example.myapplication.util.Shaders;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

public class Racket {
    private static FloatBuffer vertexBuffer;
    private static ByteBuffer drawListBuffer;
    private static final int mProgram;

    private int positionHandle;
    private int colorHandle;

    private int vPMatrixHandle;
    static final int COORDS_PER_VERTEX = 3;
    static float[] racketCoords = {
            0.0f, 0.0f, 0.0f,
            -0.75f,  -0.5f, 0.0f,
            -1.0f, -0.25f, 0.0f,
            -1.0f, 0.25f, 0.0f,
            -0.75f,  0.5f, 0.0f,
            0.75f,  0.5f, 0.0f,
            1.0f,  0.25f, 0.0f,
            1.0f,  -0.25f, 0.0f,
            0.75f,  -0.5f, 0.0f };

    private final int vertexStride = COORDS_PER_VERTEX * 4;
    private static final byte[] drawOrder = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 1 };
    private final float[] color = { 0.0f, 0.0f, 0.0f, 1.0f };
    private float mWidth, mHeight;
    private final float[] transformMatrix = new float[16];
    private final float[] rotateMatrix = new float[16];
    private final float[] translateMatrix = new float[16];
    private final float[] scaleMatrix = new float[16];

    static {
        ByteBuffer bb = ByteBuffer.allocateDirect(racketCoords.length * 4);
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put(racketCoords);
        vertexBuffer.position(0);

        drawListBuffer = ByteBuffer.allocateDirect(drawOrder.length);
        drawListBuffer.order(ByteOrder.nativeOrder());
        drawListBuffer.put(drawOrder);
        drawListBuffer.position(0);

        mProgram = Shaders.simpleProgram;
    }


    public Racket(float width, float height) {
        setSize(width, height);
        setAngle(0);
    }

    public void draw(float[] mvpMatrix , float x, float y) {
        GLES20.glUseProgram(mProgram);

        setPosition(x, y);

        Matrix.setIdentityM(transformMatrix, 0);
        Matrix.multiplyMM(transformMatrix, 0, mvpMatrix, 0, rotateMatrix, 0);
        Matrix.multiplyMM(transformMatrix, 0, transformMatrix, 0, translateMatrix, 0);
        Matrix.multiplyMM(transformMatrix, 0, transformMatrix, 0, scaleMatrix, 0);

        positionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition");
        GLES20.glEnableVertexAttribArray(positionHandle);
        GLES20.glVertexAttribPointer(positionHandle, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                vertexStride, vertexBuffer);

        colorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");
        GLES20.glUniform4fv(colorHandle, 1, color, 0);

        vPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");
        GLES20.glUniformMatrix4fv(vPMatrixHandle, 1, false, transformMatrix, 0);

        GLES20.glDrawElements(GLES20.GL_TRIANGLE_FAN, drawOrder.length, GLES20.GL_UNSIGNED_BYTE, drawListBuffer);

        GLES20.glDisableVertexAttribArray(positionHandle);
    }

    public void SetColor(float red, float green, float blue, float alpha) {
        color[0] = red;
        color[1] = green;
        color[2] = blue;
        color[3] = alpha;
    }
    public void setSize(float width, float height) {
        this.mWidth = width;
        this.mHeight = height;
        Matrix.setIdentityM(scaleMatrix, 0);
        Matrix.scaleM(scaleMatrix, 0, width / 2, height, 1);
    }

    public void setPosition(float x, float y) {
        Matrix.setIdentityM(translateMatrix, 0);
        Matrix.translateM(translateMatrix, 0, x, y, 0);
    }

    public void setAngle(float angle) {
        Matrix.setRotateM(rotateMatrix, 0, angle,0.0f, 0.0f, 1.0f);
    }

    public float getWidth() {
        return mWidth;
    }

    public float getHeight() {
        return mHeight;
    }
}
