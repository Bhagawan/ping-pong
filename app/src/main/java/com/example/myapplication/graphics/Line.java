package com.example.myapplication.graphics;

import android.opengl.GLES10;
import android.opengl.GLES20;

import com.example.myapplication.util.Shaders;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class Line {
    private  FloatBuffer vertexBuffer;
    private static final int mProgram;

    private int positionHandle;
    private int colorHandle;

    private int vPMatrixHandle;
    private static final int COORDS_PER_VERTEX = 2;

    private float[] lineCoords;

    private static final int vertexStride = COORDS_PER_VERTEX * 4;

    private final float[] color = { 1.0f, 1.0f, 1.0f, 1.0f };

    static {
        mProgram = Shaders.simpleProgram;
    }

    public Line(float firstX, float firstY, float secondX, float secondY) {
        lineCoords = new float[]{
                firstX, firstY,
                secondX, secondY
        };

        ByteBuffer bb = ByteBuffer.allocateDirect(lineCoords.length * 4);
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put(lineCoords);
        vertexBuffer.position(0);
    }

    public void draw(float[] mvpMatrix) {
        GLES20.glUseProgram(mProgram);

        positionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition");
        GLES20.glEnableVertexAttribArray(positionHandle);
        GLES20.glVertexAttribPointer(positionHandle, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                vertexStride, vertexBuffer);

        colorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");
        GLES20.glUniform4fv(colorHandle, 1, color, 0);

        vPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");
        GLES20.glUniformMatrix4fv(vPMatrixHandle, 1, false, mvpMatrix, 0);

        GLES10.glEnable(GLES10.GL_LINE_SMOOTH);
        GLES10.glLineWidth(10);
        GLES20.glDrawArrays(GLES20.GL_LINES, 0, lineCoords.length / COORDS_PER_VERTEX);


        GLES20.glDisableVertexAttribArray(positionHandle);
    }

    public void setColor(float red, float green, float blue, float alpha) {
        color[0] = red;
        color[1] = green;
        color[2] = blue;
        color[3] = alpha;
    }
}
