package com.example.myapplication.fragments;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.myapplication.R;

public class WinActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_win);
        fullscreen();

        Intent i = getIntent();
        if(i != null) {
            boolean onePlayer = i.getExtras().getBoolean("one_player");
            boolean red_wins = i.getExtras().getBoolean("red_wins");
            setHeader(onePlayer, red_wins);
        } else finish();

        AppCompatButton newGameButton = findViewById(R.id.button_win_new_game);
        newGameButton.setOnClickListener(v -> exit(false));
        AppCompatButton exitButton = findViewById(R.id.button_win_exit);
        exitButton.setOnClickListener(v -> exit(true));
    }

    private void setHeader(boolean onePlayer, boolean red_wins) {
        TextView header = findViewById(R.id.textView_win_header);
        if(onePlayer) {
            if(red_wins) {
                header.setText(getResources().getString(R.string.msg_single_win));
            } else header.setText(getResources().getString(R.string.msg_single_lose));
        } else {
            if(red_wins) {
                header.setText(getResources().getString(R.string.msg_red_win));
            } else header.setText(getResources().getString(R.string.msg_blue_win));
        }
    }

    private void exit(boolean exit) {
        Intent i = new Intent();
        Bundle b = new Bundle();
        b.putBoolean("exit", exit);
        i.putExtras(b);
        setResult(RESULT_OK, i);
        finish();
    }

    private void fullscreen() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }
}