package com.example.myapplication.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.mvp.SettingsPresenter;
import com.example.myapplication.mvp.SettingsPresenterViewInterface;
import com.example.myapplication.util.LocaleHelper;

import java.util.ArrayList;
import java.util.List;

import moxy.InjectViewState;
import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;


public class SettingsFragment extends MvpAppCompatFragment implements SettingsPresenterViewInterface {
    private View mView;

    @InjectPresenter
    SettingsPresenter mPresenter;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                getParentFragmentManager().popBackStack();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_settings, container, false);

        ImageButton backButton = mView.findViewById(R.id.imageButton_settings_back);
        backButton.setOnClickListener(v -> getParentFragmentManager().popBackStack());

        mPresenter.setup(getContext());
        return mView;
    }

    @Override
    public void fillSpinner(List<String> languages, int current) {
        Spinner spinner = mView.findViewById(R.id.spinner_settings);

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, languages);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);

        spinner.setSelection(current);

        ImageView flag = mView.findViewById(R.id.imageView_settings_flag);
        TextView header = mView.findViewById(R.id.textView_settings_header);
        TextView langText = mView.findViewById(R.id.textView_settings_language);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch(languages.get(position)) {
                    case "English":
                        LocaleHelper.setLocale(getContext(), "en");
                        flag.setImageResource(R.drawable.ic_flag_great_britain);
                        break;
                    case "Русский":
                        LocaleHelper.setLocale(getContext(), "ru");
                        flag.setImageResource(R.drawable.ic_flag_russia);
                        break;
                }
                langText.setText(getString(R.string.string_language));
                header.setText(getString(R.string.btn_settings));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}