package com.example.myapplication.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.myapplication.GameActivity;
import com.example.myapplication.R;

public class MenuFragment extends Fragment {
    private View mView;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                requireActivity().finish();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_menu, container, false);
        AppCompatButton onePlayerButton = mView.findViewById(R.id.button_menu_singleplayer);
        onePlayerButton.setOnClickListener(v -> {
            Intent i = new Intent(getContext(), GameActivity.class);
            Bundle b = new Bundle();
            b.putBoolean("one_player", true);
            i.putExtras(b);
            startActivity(i);
        });
        AppCompatButton twoPlayersButton = mView.findViewById(R.id.button_menu_multiplayer);
        twoPlayersButton.setOnClickListener(v -> {
            Intent i = new Intent(getContext(), GameActivity.class);
            Bundle b = new Bundle();
            b.putBoolean("one_player", false);
            i.putExtras(b);
            startActivity(i);
        });

        FragmentTransaction ft = getParentFragmentManager().beginTransaction();
        Button settings = mView.findViewById(R.id.button_menu_settings);
        settings.setOnClickListener(v -> ft.replace(R.id.fragmentContainerView, new SettingsFragment())
                .addToBackStack(null).commit());

        return mView;
    }
}