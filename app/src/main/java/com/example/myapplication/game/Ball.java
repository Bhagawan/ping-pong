package com.example.myapplication.game;

import com.example.myapplication.graphics.Circle;

import java.util.Random;

public class Ball {
    private final float[] mBallCoords = new float[4]; //Ball x, y Move vector x, y
    private float ballSpeed = 1;
    private Circle ball;
    private float mRadius;
    private float mSpeed;

    public Ball(float x, float y, float radius, int speed) {
        mBallCoords[0] = x;
        mBallCoords[1] = y;
        mRadius = radius;
        mSpeed = speed;
        Random r = new Random();
        mBallCoords[2] = r.nextFloat();
        mBallCoords[3] = 1 - mBallCoords[2];
        ball = new Circle(radius,360,0,0,0);
    }

    public void draw(float[] mvpMatrix) {
        ball.draw(mvpMatrix, mBallCoords[0], mBallCoords[1]);
    }

    public void setPosition(float x, float y) {
        mBallCoords[0] = x;
        mBallCoords[1] = y;
    }

    public void setVector(float x, float y) {
        mBallCoords[2] = x;
        mBallCoords[3] = y;
    }

    public void setVectorX(float x) {
        mBallCoords[2] = x;
    }

    public void setVectorY(float y) {
        mBallCoords[3] = y;
    }

    public float getCurrentX() {return mBallCoords[0];}

    public float getCurrentY() {return mBallCoords[1];}

    public float getVectorX() {return mBallCoords[2];}

    public float getVectorY() {return mBallCoords[3];}

    public float getRadius() {return mRadius;}

    public float getSpeed() {return mSpeed;}

    public void setSpeed(float speed) { mSpeed = speed;}

    public void increaseSpeed() { mSpeed += 0.02;}

    public void reset() {
        mBallCoords[0] = 0;
        mBallCoords[1] = 0;
        mSpeed = 1;
        Random r = new Random();
        mBallCoords[2] = r.nextFloat();
        mBallCoords[3] = 1 - mBallCoords[2];
    }
}
