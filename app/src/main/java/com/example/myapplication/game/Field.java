package com.example.myapplication.game;

import com.example.myapplication.graphics.Line;
import com.example.myapplication.graphics.RectWithTexture;
import com.example.myapplication.util.Textures;

public class Field {
    private final RectWithTexture background;
    private final Line topLine;
    private final Line bottomLine;
    private final float step;

    public Field(float width, float height) {
        step = height / 5;
        topLine = new Line(-width, height - step, width, height  - step);
        bottomLine = new Line(-width,  step - height, width, step - height);
        background = new RectWithTexture(width * 2, height * 2, Textures.BACK_TEXT);
    }

    public void draw(float[] mvpMatrix) {
        background.draw(mvpMatrix);
        topLine.draw(mvpMatrix);
        bottomLine.draw(mvpMatrix);
    }

    public void setColor(float red, float green, float blue, float alpha) {
        topLine.setColor(red, green, blue, alpha);
        bottomLine.setColor(red, green, blue, alpha);
    }

    public float getStep() {
        return step;
    }
}
