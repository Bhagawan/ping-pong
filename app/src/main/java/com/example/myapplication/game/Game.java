package com.example.myapplication.game;

import com.example.myapplication.graphics.Racket;
import com.example.myapplication.graphics.RectWithTexture;
import com.example.myapplication.util.Textures;

public class Game {
    private static final double PERFECT_DT = 16.67 * 2;
    private Racket racket;
    private Ball ball;
    private Field field;
    private RectWithTexture mScoreRect;
    private float mHeight;
    private float mWidth;
    private float mTranslationX, mTranslationY;
    private int scoreRed = 0, scoreBlue = 0;
    private int scoreRedBitmap;
    private int scoreBlueBitmap;
    private boolean mOnePlayer = true;
    private float mAutoRacketSpeed = 2;

    private boolean gameIsActive = true;

    private float[] mRacketCoord = new float[4]; //Top racket x, y Bottom Racket x, y
    private GameCallback callback;

    public Game() { }

    public void initialise(float screenWidth, float screenHeight, float gameWidth, float gameHeight) {
        this.mWidth = gameWidth;
        this.mHeight = gameHeight;
        mTranslationX =  (mWidth * 2) / screenWidth;
        mTranslationY = (mHeight * 2) / screenHeight;
        racket = new Racket((float) (mWidth * 0.4), mHeight / 40);
        field = new Field(mWidth, mHeight);
        mRacketCoord = new float[]{0,(racket.getHeight() / 2) + mHeight - field.getStep(), 0,  field.getStep() - mHeight - (racket.getHeight() / 2)};
        ball = new Ball(0,0, mWidth / 20,1);

        mScoreRect = new RectWithTexture(mWidth / 6, (float) ((mWidth / 6) * 1.4), Textures.TEXTURE_0);
    }

    public void newGame() {
        mRacketCoord[0] = 0;
        mRacketCoord[2] = 0;
        ball.reset();
        scoreBlue = 0;
        scoreRed = 0;
        gameIsActive = true;
    }

    public void setMode(boolean singlePlayer) {mOnePlayer = singlePlayer;}

    public void setCallback (GameCallback callback) {
        this.callback = callback;
    }

    public void moveRacket(float x, float y) {
        float moveX = x * mTranslationX - mWidth;
        if(moveX < (racket.getWidth() / 2) - mWidth) moveX = (racket.getWidth() / 2) - mWidth;
        else if(moveX > mWidth - (racket.getWidth() / 2)) moveX =  mWidth - (racket.getWidth() / 2);
        if(mOnePlayer) mRacketCoord[2] = moveX;
        else if(y * mTranslationY > mHeight) mRacketCoord[2] = moveX;
        else mRacketCoord[0] = moveX;
    }

    public void draw(float[] mVPMatrix, long dT) {
        drawField(mVPMatrix);
        drawScore(mVPMatrix);
        drawRackets(mVPMatrix);
        drawBall(mVPMatrix, dT / PERFECT_DT);
    }

    private void drawField(float[] mVPMatrix) {
        field.setColor(1.0f,1.0f,1.0f,1.0f);
        field.draw(mVPMatrix);
    }

    private void drawRackets(float[] mVPMatrix) {
        if(mOnePlayer) updateAutoRacket();

        racket.SetColor(0.0f,0.0f,1.0f,1.0f);
        racket.draw(mVPMatrix, mRacketCoord[0],mRacketCoord[1]);

        racket.SetColor(1.0f,0.0f,0.0f,1.0f);
        racket.draw(mVPMatrix, mRacketCoord[2],mRacketCoord[3]);
    }

    private void drawScore(float[] mVPMatrix) {
        setScore();
        mScoreRect.setColor(1.0f,0.0f,0.0f,1.0f);
        mScoreRect.setTexture(scoreRedBitmap);
        mScoreRect.draw(mVPMatrix,mWidth - (mScoreRect.getWidth() / 2),- mScoreRect.getHeight());
        mScoreRect.setTexture(scoreBlueBitmap);
        mScoreRect.setColor(0.0f,0.0f,1.0f,1.0f);
        mScoreRect.draw(mVPMatrix,mWidth - (mScoreRect.getWidth() / 2), mScoreRect.getHeight());
        if(gameIsActive) scoreCheck();
    }

    private void drawBall(float[] mVPMatrix, double dT) {
        if(gameIsActive) {
            updateBall((float) dT);
            ballCheck();
        }
        ball.draw(mVPMatrix);
    }

    private void updateBall(float dT) {
        float newX = ball.getCurrentX() + ball.getVectorX() * ball.getSpeed() * dT;
        float newY = ball.getCurrentY() + ball.getVectorY() * ball.getSpeed() * dT;

        if(newX >= ball.getRadius() - mWidth && newX <= mWidth - ball.getRadius()
                && newY >= field.getStep() + ball.getRadius() - mHeight && newY <= mHeight - field.getStep() - ball.getRadius()) {
            ball.setPosition(newX, newY);
        } else {
            if(ball.getVectorX() > 0) {
                float rX =  mWidth - ball.getRadius();
                float rY = (ball.getVectorY() / ball.getVectorX()) * rX - ball.getCurrentX() * (ball.getVectorY() / ball.getVectorX()) + ball.getCurrentY();
                float sX, sY;

                if(ball.getVectorY() > 0) sY = mHeight - field.getStep() - ball.getRadius();
                else sY = ball.getRadius() + field.getStep() - mHeight;

                sX = sY * (ball.getVectorX() / ball.getVectorY()) - (ball.getCurrentY() * (ball.getVectorX() / ball.getVectorY())) + ball.getCurrentX();
                if(Math.sqrt(Math.pow(rX - ball.getCurrentX(), 2) + Math.pow(rY - ball.getCurrentY(), 2)) < Math.sqrt(Math.pow(sX - ball.getCurrentX(), 2) + Math.pow(sY - ball.getCurrentY(), 2))) {
                    ball.setPosition(rX, rY);
                    ball.setVectorX(ball.getVectorX() * -1);
                } else {
                    if(ball.getVectorY() < 0) {
                        if(sX > (mRacketCoord[2] + (racket.getWidth() / 2)) || sX < (mRacketCoord[2] - (racket.getWidth() / 2))) {
                            ball.setPosition(newX, newY);
                        } else {
                            ball.setPosition(sX, sY);
                            ball.setVectorY(ball.getVectorY() * -1);
                        }
                    } else {
                        if(sX > (mRacketCoord[0] + (racket.getWidth() / 2)) || sX < (mRacketCoord[0] - (racket.getWidth() / 2))) {
                            ball.setPosition(newX, newY);
                        } else {
                            ball.setPosition(sX, sY);
                            ball.setVectorY(ball.getVectorY() * -1);
                        }
                    }
                }
            } else {
                float lX = ball.getRadius() - mWidth;
                float lY = ball.getVectorY() / ball.getVectorX() * lX - (ball.getCurrentX() * (ball.getVectorY() / ball.getVectorX())) + ball.getCurrentY();
                float sX, sY;

                if(ball.getVectorY() > 0) sY = mHeight - field.getStep() - ball.getRadius();
                else sY = field.getStep() + ball.getRadius() - mHeight;

                sX = sY * (ball.getVectorX() / ball.getVectorY()) - (ball.getCurrentY() * (ball.getVectorX() / ball.getVectorY())) + ball.getCurrentX();
                if(Math.sqrt(Math.pow(lX - ball.getCurrentX(), 2) + Math.pow(lY - ball.getCurrentY(), 2)) < Math.sqrt(Math.pow(sX - ball.getCurrentX(), 2) + Math.pow(sY - ball.getCurrentY(), 2))) {
                    ball.setPosition(lX, lY);
                    ball.setVectorX(ball.getVectorX() * -1);
                } else {
                    if(ball.getVectorY() < 0) {
                        if(sX > mRacketCoord[2] + (racket.getWidth() / 2) || sX < mRacketCoord[2] - (racket.getWidth() / 2)) {
                            ball.setPosition(newX, newY);
                        } else {
                            ball.setPosition(sX, sY);
                            ball.setVectorY(ball.getVectorY() * -1);
                        }
                    } else {
                        if(sX > mRacketCoord[0] + (racket.getWidth() / 2) || sX < mRacketCoord[0] - (racket.getWidth() / 2)) {
                            ball.setPosition(newX, newY);
                        } else {
                            ball.setPosition(sX, sY);
                            ball.setVectorY(ball.getVectorY() * -1);
                        }
                    }
                }
            }
        }
        ball.increaseSpeed();
    }

    private void ballCheck() {
        if(ball.getCurrentY() > mHeight) {
            scoreRed++;
            ball.reset();
        } else if(ball.getCurrentY() < -mHeight) {
            scoreBlue++;
            ball.reset();
        }
    }

    private void scoreCheck() {
        if(callback != null && gameIsActive) {
            if(scoreRed >= 5) {
                gameIsActive = false;
                callback.endGame(true);
            }
            else if(scoreBlue >= 5) {
                gameIsActive = false;
                callback.endGame(false);
            }
        }
    }

    private void setScore() {
        switch (scoreBlue) {
            case 0:
                scoreBlueBitmap = Textures.TEXTURE_0;
                break;
            case 1:
                scoreBlueBitmap = Textures.TEXTURE_1;
                break;
            case 2:
                scoreBlueBitmap = Textures.TEXTURE_2;
                break;
            case 3:
                scoreBlueBitmap = Textures.TEXTURE_3;
                break;
            case 4:
                scoreBlueBitmap = Textures.TEXTURE_4;
                break;
            case 5:
                scoreBlueBitmap = Textures.TEXTURE_5;
                break;
        }
        switch (scoreRed) {
            case 0:
                scoreRedBitmap = Textures.TEXTURE_0;
                break;
            case 1:
                scoreRedBitmap = Textures.TEXTURE_1;
                break;
            case 2:
                scoreRedBitmap = Textures.TEXTURE_2;
                break;
            case 3:
                scoreRedBitmap = Textures.TEXTURE_3;
                break;
            case 4:
                scoreRedBitmap = Textures.TEXTURE_4;
                break;
            case 5:
                scoreRedBitmap = Textures.TEXTURE_5;
                break;
        }
    }

    private void updateAutoRacket() {
        float dX = Math.abs(ball.getCurrentX() - mRacketCoord[0]);
        if(dX > mAutoRacketSpeed) dX = mAutoRacketSpeed;
        mRacketCoord[0] = mRacketCoord[0] + (dX * Math.signum(ball.getCurrentX() - mRacketCoord[0]));
        if(mRacketCoord[0] < (racket.getWidth() / 2) - mWidth) mRacketCoord[0] = (racket.getWidth() / 2) - mWidth;
        else if(mRacketCoord[0] > mWidth - (racket.getWidth() / 2)) mRacketCoord[0] = mWidth - (racket.getWidth() / 2);
    }

    public interface GameCallback {
        void endGame(boolean redPlayerWins);
    }
}
