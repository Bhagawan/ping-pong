package com.example.myapplication;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.example.myapplication.fragments.WinActivity;
import com.example.myapplication.graphics.MyGLSurfaceView;
import com.example.myapplication.mvp.GamePresenter;
import com.example.myapplication.mvp.GamePresenterViewInterface;
import com.example.myapplication.util.Textures;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import moxy.MvpAppCompatActivity;
import moxy.presenter.InjectPresenter;

public class GameActivity extends MvpAppCompatActivity implements GamePresenterViewInterface {

    @InjectPresenter
    GamePresenter mPresenter;

    private MyGLSurfaceView mGLView;
    private boolean onePlayer = true;

    private final ActivityResultLauncher<Intent> mLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
        if(result != null) {
            boolean exit = result.getData().getBooleanExtra("exit",true);
            if (exit) finish();
            else mGLView.newGame();
        }
    });

    @Override
    public void onBackPressed() {finish(); }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        mGLView = findViewById(R.id.myGLSurfaceView);

        Intent i = getIntent();
        if(i != null) onePlayer = i.getExtras().getBoolean("one_player");

        getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(visibility -> fullscreen());
        fullscreen();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mGLView != null) {
            mGLView.onResume();
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        if (mGLView != null) {
            mGLView.onPause();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        fullscreen();
        return super.dispatchTouchEvent(event);
    }

    @Override
    protected void onDestroy() {
        Textures.reset();
        super.onDestroy();
    }

    private void winScreen(boolean redPlayerWins) {
        Intent i = new Intent(this, WinActivity.class);
        Bundle b = new Bundle();
        b.putBoolean("one_player", onePlayer);
        b.putBoolean("red_wins", redPlayerWins);
        i.putExtras(b);
        mLauncher.launch(new Intent(i));
    }

    private void fullscreen() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    @Override
    public void runGame() {
        mGLView.setGameMode(onePlayer);
        mGLView.setCallback(new MyGLSurfaceView.GameInterface() {
            @Override
            public void onWin(boolean redPlayerWins) {
                winScreen(redPlayerWins);
            }

            @Override
            public void onTexturesLoaded() {
                hideLoadingScreen();
            }
        });
        mGLView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoadingScreen() {
        ConstraintLayout cl = findViewById(R.id.layout_loading_screen);
        cl.setVisibility(View.VISIBLE);
        TextView sub = findViewById(R.id.textView_loading);
        sub.setText(getApplicationContext().getResources().getString(R.string.msg_loading_textures));
    }

    public void hideLoadingScreen() {
        ConstraintLayout cl = findViewById(R.id.layout_loading_screen);
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());
        executor.execute(() -> handler.post(() -> cl.setVisibility(View.GONE)));
    }
}