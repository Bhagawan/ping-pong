package com.example.myapplication.mvp;

import android.content.Context;

import com.example.myapplication.R;
import com.example.myapplication.util.LocaleHelper;

import java.util.ArrayList;
import java.util.List;

import moxy.InjectViewState;
import moxy.MvpPresenter;

@InjectViewState
public class SettingsPresenter extends MvpPresenter<SettingsPresenterViewInterface> {

    public void setup(Context context) {
        List<String> languages = new ArrayList<>();
        languages.add(context.getResources().getString(R.string.string_english));
        languages.add(context.getResources().getString(R.string.string_russian));
        int current = 0;
        String l = LocaleHelper.getLanguage(context);
        switch (l) {
            case "en" :
                current = 0;
                break;
            case "ru" :
                current = 1;
                break;
        }
        getViewState().fillSpinner(languages, current);
    }
}
