package com.example.myapplication.mvp;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import com.example.myapplication.util.Textures;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import moxy.InjectViewState;
import moxy.MvpPresenter;

@InjectViewState
public class GamePresenter extends MvpPresenter<GamePresenterViewInterface> {
    private Target target;

    @Override
    protected void onFirstViewAttach() {
        getViewState().showLoadingScreen();
        if(!Textures.backgroundLoaded()) {
            target = new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    Textures.setBackground(bitmap);
                    getViewState().runGame();
                }

                @Override
                public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                    getViewState().runGame();
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            };
            Picasso.get().load("http://195.201.125.8/PingPongApp/background.png").into(target);
        } else getViewState().runGame();
    }
}
