package com.example.myapplication.mvp;

import java.util.List;

import moxy.MvpView;
import moxy.viewstate.strategy.alias.OneExecution;
import moxy.viewstate.strategy.alias.SingleState;

public interface SettingsPresenterViewInterface extends MvpView {
    @SingleState
    void fillSpinner(List<String> languages, int current);
}
