package com.example.myapplication.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.opengl.GLES20;
import android.opengl.GLUtils;

import com.example.myapplication.R;

public class Textures {
    public static int BACK_TEXT;
    public static int TEXTURE_0, TEXTURE_1, TEXTURE_2, TEXTURE_3, TEXTURE_4 ,TEXTURE_5;
    private static Bitmap background;

    public static int loadTexture(Context context, int resourceId) {
        int[] textureHandle = new int[1];

        GLES20.glGenTextures(1, textureHandle, 0);

        if (textureHandle[0] != 0) {
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inScaled = false;
            final Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resourceId, options);

            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle[0]);

            GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
            GLES20.glEnable(GLES20.GL_BLEND);

            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

            GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);

            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
            bitmap.recycle();
        }

        if (textureHandle[0] == 0) {
            throw new RuntimeException("Error loading texture.");
        }

        return textureHandle[0];
    }

    public static int loadTexture(Bitmap bitmap) {
        int[] textureHandle = new int[1];

        GLES20.glGenTextures(1, textureHandle, 0);

        if (textureHandle[0] != 0) {
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inScaled = false;

            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle[0]);

            GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
            GLES20.glEnable(GLES20.GL_BLEND);

            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

            GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);

            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
            if(bitmap != background) bitmap.recycle();
        }

        if (textureHandle[0] == 0) {
            throw new RuntimeException("Error loading texture.");
        }

        return textureHandle[0];
    }

    private static boolean loaded = false;

    public static void load(Context context) {
        if (!loaded) {
            if(background != null) BACK_TEXT = loadTexture(background);
            else BACK_TEXT = loadTexture(Util.createColorBitmap(64,64, context.getResources().getColor(R.color.default_back)));
            TEXTURE_0 = loadTexture(Util.createTxtImage("0", 50, Color.WHITE));
            TEXTURE_1 = loadTexture(Util.createTxtImage("1", 50, Color.WHITE));
            TEXTURE_2 = loadTexture(Util.createTxtImage("2", 50, Color.WHITE));
            TEXTURE_3 = loadTexture(Util.createTxtImage("3", 50, Color.WHITE));
            TEXTURE_4 = loadTexture(Util.createTxtImage("4", 50, Color.WHITE));
            TEXTURE_5 = loadTexture(Util.createTxtImage("5", 50, Color.WHITE));
            loaded = true;
        }
    }

    public static void setBackground(Bitmap background) {
        Textures.background = background;
    }

    public static boolean isLoaded() { return Textures.loaded;}

    public static void reset() {
        loaded = false;
    }

    public static boolean backgroundLoaded() {
        return background != null;
    }

}
