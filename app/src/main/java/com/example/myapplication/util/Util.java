package com.example.myapplication.util;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

public class Util {
    public static Bitmap createTxtImage(String string, int txtSize, int color) {
        Bitmap bitmap = Bitmap.createBitmap(string.length() * txtSize + 4,
                txtSize + 4, Bitmap.Config.ARGB_8888);
        Canvas canvasTemp = new Canvas(bitmap);
        Paint p = new Paint();
        p.setAntiAlias(true);
        p.setColor(color);
        p.setTextSize(txtSize);
        canvasTemp.drawText(string, 2, txtSize - 2, p);
        return bitmap;
    }

    public static Bitmap createColorBitmap(int width, int height, int color) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawColor(color);
        return bitmap;
    }
}
