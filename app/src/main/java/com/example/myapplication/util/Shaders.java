package com.example.myapplication.util;

import android.opengl.GLES20;

public class Shaders {
    public static int textureProgram, simpleProgram;
    public static final String VERTEX_SHADER =
            "uniform mat4 uMVPMatrix;" +
                    "attribute vec4 vPosition;" +
                    "void main() {" +
                    "  gl_Position = uMVPMatrix * vPosition;" +
                    "}";

    public static final String FRAGMENT_SHADER =
            "precision mediump float;" +
                    "uniform vec4 vColor;" +
                    "void main() {" +
                    "  gl_FragColor = vColor;" +
                    "}";

    public static final String VERTEX_TEXTURE_SHADER =
            "uniform mat4 uMVPMatrix;" +
                    "attribute vec4 aPosition;" +
                    "attribute vec2 aTexCoordinate;" +
                    "varying vec2 vTexCoordinate;" +
                    "void main() {" +
                    "  vTexCoordinate = aTexCoordinate;" +
                    "  gl_Position = uMVPMatrix * aPosition;" +
                    "}";

    public static final String FRAGMENT_TEXTURE_SHADER =
            "precision mediump float;" +
                    "uniform lowp vec4 vColor;" +
                    "uniform sampler2D uTexture;" +
                    "varying vec2 vTexCoordinate;" +
                    "void main() {" +
                    "  gl_FragColor = vColor * texture2D(uTexture, vTexCoordinate);" +
                    "}";

    public static int loadShader(int type, String shaderCode){
        int shader = GLES20.glCreateShader(type);
        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);
        return shader;
    }

    public static int createProgram() {
        int vertexShader = Shaders.loadShader(GLES20.GL_VERTEX_SHADER, Shaders.VERTEX_SHADER);
        int fragmentShader = Shaders.loadShader(GLES20.GL_FRAGMENT_SHADER, Shaders.FRAGMENT_SHADER);

        int mProgram = GLES20.glCreateProgram();

        GLES20.glAttachShader(mProgram, vertexShader);

        GLES20.glAttachShader(mProgram, fragmentShader);

        GLES20.glLinkProgram(mProgram);
        return mProgram;
    }

    public static void createShaders() {
        int vertexShader = Shaders.loadShader(GLES20.GL_VERTEX_SHADER, Shaders.VERTEX_TEXTURE_SHADER);
        int fragmentShader = Shaders.loadShader(GLES20.GL_FRAGMENT_SHADER, Shaders.FRAGMENT_TEXTURE_SHADER);

        int mProgram = GLES20.glCreateProgram();

        GLES20.glAttachShader(mProgram, vertexShader);

        GLES20.glAttachShader(mProgram, fragmentShader);

        GLES20.glLinkProgram(mProgram);
        textureProgram = mProgram;

        vertexShader = Shaders.loadShader(GLES20.GL_VERTEX_SHADER, Shaders.VERTEX_SHADER);
        fragmentShader = Shaders.loadShader(GLES20.GL_FRAGMENT_SHADER, Shaders.FRAGMENT_SHADER);

        mProgram = GLES20.glCreateProgram();

        GLES20.glAttachShader(mProgram, vertexShader);

        GLES20.glAttachShader(mProgram, fragmentShader);

        GLES20.glLinkProgram(mProgram);
        simpleProgram = mProgram;
    }
}
